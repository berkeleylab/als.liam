# -*- coding: utf-8 -*-

import sip
sip.setapi('QVariant', 2)	# Call this before referencing QtCore

# from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import Qt, QString, QVariant, QRect, QRectF, QSize, QPoint
from PyQt4.QtCore import QAbstractTableModel, QModelIndex
from PyQt4.QtGui import QApplication, QMainWindow, QTextEdit, QLabel, QStyle
from PyQt4.QtGui import QStyledItemDelegate, QStyleOptionViewItemV4
from PyQt4.QtGui import QTextDocument, QPushButton, QStyleOptionButton
from PyQt4.QtGui import QStyleFactory
# from PyQt4.QtGui import QCommonStyle, QCleanlooksStyle, QPlastiqueStyle
# from PyQt4.QtGui import QMacStyle, QWindowsStyle, QWindowsXPStyle
import sys
import qt_rluCut

from numpy import nan, float32, sqrt
import pandas as pd

try:
    _fromUtf8 = QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QApplication.translate(context, text, disambig)

class qhvModel(QAbstractTableModel):
	def __init__(self, parent=None):
		super(qhvModel, self).__init__(parent)
		
		self.numRows = 4
		self.numCols = 5
		
		self.labels = [
			"q<sub>x</sub>", 
			"q<sub>y</sub>", 
			"q<sub>z</sub>", 
			"|q|", 
			]
		self.values = [
			1., 
			1., 
			1., 
			sqrt(3.), 
			]
		self.units = [
			"nm<sup>-1</sup>", 
			"nm<sup>-1</sup>", 
			"nm<sup>-1</sup>", 
			"nm<sup>-1</sup>", 
			]

		self.xfer_values = [
			">", 
			">", 
			">", 
			">", 
			]

		self.xfer_all_values = [
			"q>", 
			"q>", 
			"q>", 
			"q>", 
			]
		
		(
			self.labels_col, 
			self.values_col, 
			self.units_col, 
			self.xfer_values_col, 
			self.xfer_all_values_col, 
			) = range(self.numCols)
		
		self.df = pd.DataFrame(
			{
				"labels": self.labels, 
				"values": self.values, 
				"units": self.units, 
				"xfer_values": self.xfer_values, 
				# "xfer_all_values": [nan]*self.numRows, 
				"xfer_all_values": self.xfer_all_values, 
				}, 
			)
		self.df = self.df.reindex_axis([
			"labels", 
			"values", 
			"units", 
			"xfer_values", 
			"xfer_all_values", 
			], axis=1)
		# print self.df["values"]
		# self.df["values"] = self.df["values"].astype(float32)
		# print self.df["values"]
		# print self.df
	
	def rowCount(self, parent=None):
		if (parent is None) or (parent == QModelIndex()):
			return(self.numRows)
		return(0)
	
	def columnCount(self, parent=None):
		if (parent is None) or (parent == QModelIndex()):
			return(self.numCols)
		return(0)
	
	def data(self, index, role = Qt.DisplayRole):
		if (role == Qt.DisplayRole):
			return self.df.iloc[index.row(), index.column()]
		# return QVariant()
		return None

class qtLabelDelegate(QStyledItemDelegate):
	def __init__(self, parent=None):
		super(QStyledItemDelegate, self).__init__(parent)
	
	def paint (self, painter, option, index):
		return
# 		# item = QLabel(QString(index.data()), self)
# 		# item = QLabel(str(index.data()), self)
# 		item = QLabel(index.data().toString(), self.parent())
# 		item.setDisabled(True)
# 		item.setAutoFillBackground(True)
# 		# item.render(painter)
# 		item.drawContents(painter)
	
	# update sizeHint() ?

class qtHtmlDelegate(QStyledItemDelegate):
	def __init__(self, parent=None):
		super(QStyledItemDelegate, self).__init__(parent)
	
	def paint(self, painter, option, index):
		options = QStyleOptionViewItemV4(option)
		self.initStyleOption(options, index)
		
		painter.save()
		
		doc = QTextDocument()
		doc.setHtml(options.text)
		
		options.text = ""
		options.widget.style().drawControl(
			QStyle.CE_ItemViewItem, options, painter)
		
		# Assuming .alignment() & Qt.AlignVCenter
		doc_size = self.sizeHint(option, index)
		height_offset = (options.rect.height() - doc_size.height()) / 2.
# 		print "\n", index.row(), ",", index.column(), ":"
# 		print "options.rect.height():", options.rect.height()
# 		print "doc_size.height():", doc_size.height()
# 		print "height_offset:", height_offset
# 		print "doc_size.width():", doc_size.width()
		height_offset = max(0, height_offset)
		painter.translate(
			options.rect.left(), 
			options.rect.top() + height_offset)
		clip = QRectF(0, 0, options.rect.width(), options.rect.height())
		doc.drawContents(painter, clip)
		
		painter.restore()
	
	def sizeHint(self, option, index):
		options = QStyleOptionViewItemV4(option)
		self.initStyleOption(options, index)
		
		doc = QTextDocument()
		doc.setHtml(options.text)
		# doc.setTextWidth(options.rect.width())	# For multiline text
		return QSize(doc.idealWidth(), doc.size().height())

class qtFloatDelegate(QStyledItemDelegate):
	def __init__(self, parent=None, decimals=6):
		super(QStyledItemDelegate, self).__init__(parent)
		self.decimals = decimals
	
# 	def paint(self, painter, option, index):
# 		painter.save()
# 		
# 		# value = float(index.data().toPyObject())
# 		value = float(index.data())
# 		# painter.drawText(
# 		# 	option.rect, 
# 		# 	Qt.AlignRight & Qt.AlignVCenter, 
# 		# 	"{0:0.{1:d}f}".format(value, self.decimals))
# 		
# 		# Assuming .alignment() & Qt.AlignRight & Qt.AlignVCenter
# 		doc_size = self.sizeHint(option, index)
# 		height_offset = (option.rect.height() - doc_size.height()) / 2.
# 		print "\n", index.row(), ",", index.column(), ":"
# 		print "option.rect.height():", option.rect.height()
# 		print "doc_size.height():", doc_size.height()
# 		print "height_offset:", height_offset
# 		print "doc_size.width():", doc_size.width()
# 		height_offset = max(0, height_offset)
# 		height_offset = 0
# 		painter.translate(
# 			# option.rect.right() - doc_size.width(), 
# 			option.rect.left(), 
# 			# option.rect.left() + doc_size.width()/2., 
# 			option.rect.top() + height_offset)
# 		painter.drawText(
# 			option.rect, 
# 			# Qt.AlignRight & Qt.AlignVCenter, 
# 			Qt.AlignLeft & Qt.AlignTop, 
# 			"{0:0.{1:d}f}".format(value, self.decimals))
# 		
# 		painter.restore()
	
	def paint(self, painter, option, index):
		options = QStyleOptionViewItemV4(option)
		self.initStyleOption(options, index)
		
		painter.save()
		
		doc = QTextDocument()
		# value = float(index.data().toPyObject())
		value = float(index.data())
		doc.setPlainText("{0:0.{1:d}f}".format(value, self.decimals))
		
		options.text = ""
		options.widget.style().drawControl(
			QStyle.CE_ItemViewItem, options, painter)
		
		# Assuming .alignment() & Qt.AlignRight & Qt.AlignVCenter
		doc_size = self.sizeHint(option, index)
		height_offset = (options.rect.height() - doc_size.height()) / 2.
# 		print "\n", index.row(), ",", index.column(), ":"
# 		print "options.rect.height():", options.rect.height()
# 		print "doc_size.height():", doc_size.height()
# 		print "height_offset:", height_offset
# 		print "doc_size.width():", doc_size.width()
		height_offset = max(0, height_offset)
		painter.translate(
			options.rect.right() - doc_size.width(), 
			options.rect.top() + height_offset)
		clip = QRectF(0, 0, options.rect.width(), options.rect.height())
		doc.drawContents(painter, clip)
		
		painter.restore()
	
	def sizeHint(self, option, index):
		options = QStyleOptionViewItemV4(option)
		self.initStyleOption(options, index)
		# print "index ({0:d}, {1:d}): data =".format(index.row(), index.column()), index.data().toPyObject()
		# print "index ({0:d}, {1:d}): data =".format(index.row(), index.column()), index.data()
		
		doc = QTextDocument()
		# value = float(index.data().toPyObject())
		value = float(index.data())
		doc.setPlainText("{0:0.{1:d}f}".format(value, self.decimals))
		# doc.setTextWidth(options.rect.width())	# For multiline text
		return QSize(doc.idealWidth(), doc.size().height())

class qtButtonDelegate(QStyledItemDelegate):
	def __init__(self, parent=None, decimals=6):
		super(QStyledItemDelegate, self).__init__(parent)
	
	def paint(self, painter, option, index):
		# options = QStyleOptionViewItemV4(option)
		# self.initStyleOption(options, index)
		
		painter.save()
		
		doc = QPushButton(index.data(), self.parent() )
		
		# options.text = ""
		# options.widget.style().drawControl(
		# 	QStyle.CE_PushButton, options, painter)
		btn_option = QStyleOptionButton()
		# btn_option.initFrom(option)
		# print "btn_option.state:", type(btn_option.state), btn_option.state
		# print "QStyle.State_Enabled:", type(QStyle.State_Enabled), QStyle.State_Enabled
		# print "QStyle.State_Raised:", type(QStyle.State_Raised), QStyle.State_Raised
		print "btn_option.features:", btn_option.features
		# btn_option.features = QStyleOptionButton.None
		# btn_option.features = QStyleOptionButton.Flat
		btn_option.state = QStyle.State_Enabled or QStyle.State_Raised
		btn_option.state = QStyle.State_Raised
		btn_option.rect = option.rect
		btn_option.text = index.data()
		doc.resize(doc.minimumSizeHint())
		
		# Assuming .alignment() & Qt.AlignCenter
		doc_size = self.sizeHint(option, index)
		btn_option.rect = QRect(
			QPoint(option.rect.left(), option.rect.top()), doc_size)
		btn_option.rect = QRect(
			QPoint(option.rect.left()+5, option.rect.top()+5), 
			QSize(doc_size.width()-10, doc_size.height()-10) )
		btn_option.rect = option.rect.adjusted(1, 1, -1, -1)
		btn_option.rect = option.rect.adjusted(2, 2, -2, -2)
		btn_option.rect = option.rect.adjusted(15, 5, -15, -5)
		height_offset = (option.rect.height() - doc_size.height()) / 2.
		width_offset = (option.rect.width() - doc_size.width()) / 2.
		print "\n", index.row(), ",", index.column(), ":"
		print "option.rect.height():", option.rect.height()
		print "doc_size.height():", doc_size.height()
		print "height_offset:", height_offset
		print "doc_size.width():", doc_size.width()
		height_offset = max(0, height_offset)
		width_offset = max(0, width_offset)
# 		painter.translate(
# 			option.rect.left() + width_offset, 
# 			option.rect.top() + height_offset)
# 		clip = QRectF(0, 0, option.rect.width(), option.rect.height())
		# doc.drawContents(painter, clip)
		# doc.style().drawControl(
		QApplication.style().drawControl(
			QStyle.CE_PushButton, btn_option, painter)
		
		painter.restore()
	
	def sizeHint(self, option, index):
		# options = QStyleOptionViewItemV4(option)
		# self.initStyleOption(options, index)
		# print "index ({0:d}, {1:d}): data =".format(index.row(), index.column()), index.data().toPyObject()
		print "index ({0:d}, {1:d}): data =".format(index.row(), index.column()), index.data()
		
		doc = QPushButton(index.data())
		print "doc.sizeHint():", doc.sizeHint()
		print "doc.minimumSizeHint():", doc.minimumSizeHint()
		doc.resize(doc.minimumSizeHint())
		print "doc.size():", doc.size()
		print "doc.rect():", doc.rect().left(), doc.rect().top(), doc.rect().right(), doc.rect().bottom()
		print "doc.contentsRect():", doc.contentsRect().left(), doc.contentsRect().top(), doc.contentsRect().right(), doc.contentsRect().bottom()
		print "doc.contentsMargins():", doc.contentsMargins().left(), doc.contentsMargins().top(), doc.contentsMargins().right(), doc.contentsMargins().bottom()
		# doc.setTextWidth(options.rect.width())	# For multiline text
		# return QSize(doc.idealWidth(), doc.size().height())
		return doc.size()

class BuildRluCutApp(QMainWindow, qt_rluCut.Ui_MainWindow):
	def __init__(self, parent=None):
		super(BuildRluCutApp, self).__init__(parent)
		self.setupUi(self)
		
		self.model_qhv_initial = qhvModel()
	
		# tbl_init = self.table_qhv_initial
		item = self.table_qhv_initial.item(0, 0)
		item.setText(_translate("MainWindow", "q<sub>X</sub>", None))
		item = self.table_qhv_initial.item(0, 2)
		item.setText(_translate("MainWindow", "nm<sup>-1</sup>", None))
		
		# view_init = self.view_qhv_initial
		
		self.view_qhv_initial.setModel(self.model_qhv_initial)
		# print self.model_qhv_initial.rowCount()
		self.view_qhv_initial.setItemDelegateForColumn(
			0, 
			# qtLabelDelegate(self) )
			qtHtmlDelegate(self) )
		self.view_qhv_initial.setItemDelegateForColumn(
			1, 
			qtFloatDelegate(self) )
		self.view_qhv_initial.setItemDelegateForColumn(
			2, 
			# qtLabelDelegate(self) )
			qtHtmlDelegate(self) )
# 		self.view_qhv_initial.setItemDelegateForColumn(
# 			3, 
# 			qtButtonDelegate(self) )
		
		# for col in range(self.model_qhv_initial.colCount() ):
# 		for col in [0, 2]:
# 			for row in range(self.model_qhv_initial.rowCount() ):
# 				index = self.model_qhv_initial.index(row, col)
# 				# item = QTextEdit(
# 				item = QLabel(
# 					self.model_qhv_initial.data(index),
# 					# index.data(), 
# 					self.view_qhv_initial)
# 				# item.setAutoFillBackground(True)
# 				item.setDisabled(True)
# 				self.view_qhv_initial.setIndexWidget(
# 					index, 
# 					item)

# 		for col in [3]:
# 			for row in range(self.model_qhv_initial.rowCount() ):
# 				index = self.model_qhv_initial.index(row, col)
# 				delegate = self.view_qhv_initial.itemDelegate(index)
# 				print "delegate.objectName():", delegate.objectName(), type(delegate)
# 				# col_delegate = self.view_qhv_initial.itemDelegateForColumn(col)
# 				# print "col_delegate.objectName():", col_delegate.objectName()
# 				# item = self.view_qhv_initial.indexWidget(index)
# 				# print "item.objectName():", item.objectName(), type(item)
# 				
# 				style_delegate = QStyledItemDelegate(self)
# 				# options = QStyleOptionViewItemV4(option)
# 				options = QStyleOptionViewItemV4()
# 				style_delegate.initStyleOption(options, index)
# 				options.text = self.model_qhv_initial.data(index)
# 				
# 				doc = QTextDocument()
# 				doc.setPlainText(options.text)
# 				# doc.setTextWidth(options.rect.width())	# For multiline text
# 				print "doc.size():", doc.size()
# 				print "\t:", doc.size().width(), doc.size().height()
# 				print "\tnew:", doc.size().width()+10, doc.size().height()+5
# 				item = QPushButton(
# 					self.model_qhv_initial.data(index),
# 					# index.data(), 
# 					self.view_qhv_initial)
# 				# item.setAutoFillBackground(True)
# 				# item.setDisabled(True)
# 				self.view_qhv_initial.setIndexWidget(
# 					index, 
# 					item)
		
		self.view_qhv_initial.resizeColumnsToContents()

		for col in [3]:
			for row in range(self.model_qhv_initial.rowCount() ):
				index = self.model_qhv_initial.index(row, col)
				item = QPushButton(
					self.model_qhv_initial.data(index),
					# index.data(), 
					self.view_qhv_initial)
				# item.setAutoFillBackground(True)
				# item.setDisabled(True)
				self.view_qhv_initial.setIndexWidget(
					index, 
					item)
		
		(row, col) = (0, 4)
		self.view_qhv_initial.setSpan(row, col, 4, 1)
		index = self.model_qhv_initial.index(row, col)
		item = QPushButton(
			self.model_qhv_initial.data(index),
			# index.data(), 
			self.view_qhv_initial)
		# item.setAutoFillBackground(True)
		# item.setDisabled(True)
		self.view_qhv_initial.setIndexWidget(
			index, 
			item)
	
	# def setModel_qhv_initial(self, model):
	# 	self.view_qhv_initial.setModel(model)

def main():
	app = QApplication(sys.argv)
	print "QStyleFactory.keys():", [str(s) for s in QStyleFactory.keys()]
	print "app.style():", app.style(), type(app.style())
	print QApplication.style().metaObject().className(), "\n"
# 	for qtStyle in [QCommonStyle, QCleanlooksStyle, QPlastiqueStyle, QMacStyle, QWindowsStyle, QWindowsXPStyle]:
# 		print qtStyle, ":", isinstance(app.style(), qtStyle)
	for key in QStyleFactory.keys():
		st = QStyleFactory.create(key)
		print key, st.metaObject().className(), type(app.style())
	# exit()
	# app.setStyle("Windows")
	form = BuildRluCutApp()
	# model_qhv_initial = qhvModel()
	# form.view_qhv_initial.setModel(model_qhv_initial)
	form.show()
	app.exec_()

if __name__ == '__main__':
    main()