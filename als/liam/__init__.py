#!/usr/bin/env python

__author__ = "Padraic Shafer"
__copyright__ = "Copyright (c) 2017-2019, Padraic Shafer"
__credits__ = [__author__, ]
__license__ = "BSD-3-Clause-LBNL"
__maintainer__ = "Padraic Shafer"
__email__ = "PShafer@lbl.gov"
__status__ = "Development"

from .version import __version__, __date__, __credits__
